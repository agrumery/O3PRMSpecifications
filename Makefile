# ============================================================
# makefile for easy LaTeX compilation
# CG -- version du 21/03/2005
# ============================================================

.PHONY : clean 

LATEX=pdflatex -shell-escape

full: 
	${LATEX} o3prm-reference.tex &\
	${LATEX} o3prm-reference.tex

clean:
	-rm -f  *~ *idx *ilg *ind o3prm-reference.pdf \
	*.aux *.log *.out *.toc *.bbl *blg

